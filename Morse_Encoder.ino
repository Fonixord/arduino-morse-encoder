//char information[20];
int outputPin = 11;
int tM = 100;
int dashTime = 3 * tM;
int dotTime = 1 * tM;
int betweenSymbol = 1 * tM;
int betweenLetter = 5 * tM;
int betweenWord = 6*tM;
char information;
char x;
void setup() {
  pinMode(outputPin, OUTPUT);
  Serial.begin(9600);
}

void loop() {
  if(Serial.available() > 0){
  char information = Serial.read();
  if(information != 10){
  Serial.println(information);
  switch (information) {
    case 'a': s(); l(); break;
    case 'b': l(); s(); s(); s(); break;
    case 'c': l(); s(); l(); s(); break;
    case 'd': l(); s(); s(); break;
    case 'e': s(); break;
    case 'f': s(); s(); l(); s(); break;
    case 'g': l(); l(); s(); break;
    case 'h': s(); s(); s(); s(); break;
    case 'i': s(); s(); break;
    case 'j': s(); l(); l(); l(); break;
    case 'k': l(); s(); l(); break;
    case 'l': s(); l(); s(); s(); break;
    case 'm': l(); l(); break;
    case 'n': l(); s(); break;
    case 'o': l(); l(); l(); break;
    case 'p': s(); l(); l(); s(); break;
    case 'q': l(); l(); s(); l(); break;
    case 'r': s(); l(); s(); break;
    case 's': s(); s(); s(); break;
    case 't': l(); break;
    case 'u': s(); s(); l(); break;
    case 'v': s(); s(); s(); l(); break;
    case 'w': s(); l(); l(); break;
    case 'x': l(); s(); s(); l(); break;
    case 'y': l(); s(); l(); l(); break;
    case 'z': l(); l(); s(); s(); break;
    case ' ': digitalWrite(outputPin, LOW); delay(betweenWord);Serial.println("New word");break;
    default: Serial.println("Default case"); break;
   }
   betweenLetterPause();
  }
  }
}

void l(){
  digitalWrite(outputPin, HIGH);
  delay(dashTime);
  digitalWrite(outputPin, LOW);
  delay(betweenSymbol);
  Serial.println("dash");
}
void s(){
  digitalWrite(outputPin, HIGH);
  delay(dotTime);
  digitalWrite(outputPin, LOW);
  delay(betweenSymbol);
  Serial.println("dot");
}
void betweenLetterPause(){
  digitalWrite(outputPin, LOW);
  delay(betweenLetter);
  Serial.println("New letter");
}
